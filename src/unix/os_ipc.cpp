#include <climits>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "unix/os_ipc.h"

#include <algorithm>

namespace IPC {

inline int stringToInteger(const char *identifier) {
    char *end;
    long  l;
    size_t str_length = ::strnlen(identifier, IPC_IDENTIFIER_LENGTH);
    if (str_length == IPC_IDENTIFIER_LENGTH) {
        return 0;
    }
    errno = 0;
    l = ::strtol(identifier, &end, 10);
    if (l >= INT_MAX || l <= INT_MIN || errno == ERANGE) {
        return 0;
    } else {
        return static_cast<int>(l);
    }
}

Stream::Stream(const Stream &other):
    stream_(NULL)
{
    if(other.stream_ != NULL) {
        int fd = fileno(other.stream_);
        stream_ = ::fdopen(fd, "r+");
    }
    memcpy(identifier_, other.identifier_, sizeof(identifier_));
}

const Stream &Stream::operator=(const Stream &other) {
    if (&other != this) {
        Stream tmp(other);
        std::swap(stream_, tmp.stream_);
        std::swap(identifier_, tmp.identifier_);
    }
    return *this;
}

ErrorCode Stream::write(const char *data, size_t bytes) {
    if (stream_ == NULL) {
        return kStateError;
    }
    size_t written = ::fwrite(data, 1, bytes, stream_);
    if(written != bytes) {
        return kErrDataLeft;
    }
    ::fflush(stream_);
    return kSuccess;
}

ErrorCode Stream::read(char *buffer, size_t to_read) {
    if (stream_ == NULL) {
        return kStateError;
    }
    size_t red = ::fread(buffer, 1, to_read, stream_ );
    if (red != to_read) {
        return kErrDataLeft;
    }
    return kSuccess;
}

#ifndef IPC_CLIENT_ONLY
ErrorCode Stream::create() {
    int fd[2];
    if (0 != ::socketpair(AF_UNIX, SOCK_STREAM, 0, fd)) {
        return kOSError;
    }
    if (NULL == (stream_ = ::fdopen(fd[0], "r+"))) {
        return kOSError;
    }
    if(::snprintf(identifier_, IPC_IDENTIFIER_LENGTH, "%d", fd[1]) == IPC_IDENTIFIER_LENGTH) {
        identifier_[0] = '\0';
        return kErrStringTooLong;
    }
    return kSuccess;
}
#endif

#ifndef IPC_SERVER_ONLY
ErrorCode Stream::open(const char *identifier) {
    if(identifier == NULL) {
        return kErrInvalidArgument;
    }
    int fd = 0;
    // Convert char array to integer
    if (0 == (fd = stringToInteger(identifier))) {
        return kErrStringTooLong;
    }
    if (NULL == (stream_ = ::fdopen(fd, "r+"))) {
        return kOSError;
    }
    return kSuccess;
}
#endif
void Stream::close() {
    if (stream_ != NULL) {
        ::fclose(stream_);
        stream_ = NULL;
    }
}

}  // namespace IPC
