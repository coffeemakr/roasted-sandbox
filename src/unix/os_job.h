/*----------------------------------------------------------------------------*
 * Copyright (C) 2015 c0ff3m4kr. <l34k@bk.ru>                                 *
 *----------------------------------------------------------------------------*/
#ifndef SANDBOX_UNIX_SANDBOX_H_
#define SANDBOX_UNIX_SANDBOX_H_

#include "global.h"
#include "jobbase.h"

namespace sandbox {

class Job:
    public JobBase {

private:
    unsigned int active_process_limit_;
    unsigned int user_time_limit_;
    size_t memory_limit_;

protected:
    ErrorCode setup();
    ErrorCode teardown();
    ErrorCode addProcess(void* pProcess);

public:

    Job():
        active_process_limit_(0),
        user_time_limit_(0),
        memory_limit_(0)
        {}

    ~Job(){}

    inline void setActiveProcessLimit(unsigned int limit) {
        active_process_limit_ = limit;
    }
    inline void removeActiveProcessLimit() {
        active_process_limit_ = 0;
    }
    inline unsigned int activeProcessLimit() const{
        return active_process_limit_;
    }
    inline void setUserTimeLimit(unsigned int limit_100ms) {
        user_time_limit_ = limit_100ms;
    }
    inline void removeUserTimeLimit() {
        user_time_limit_ = 0;
    }
    inline unsigned int userTimeLimit() const {
        return user_time_limit_;
    }
    inline void setMemoryLimit(size_t limit) {
        memory_limit_ = limit;
    }
    inline void removeMemoryLimit() {
        memory_limit_ = 0;
    }
    inline size_t memoryLimit() const {
        return memory_limit_;
    }
    ErrorCode execute(const char *path, char * const argv[]);
};
}  // namespace sandbox

#endif  // SANDBOX_UNIX_SANDBOX_H_
