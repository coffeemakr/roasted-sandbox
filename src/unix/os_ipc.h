/*----------------------------------------------------------------------------*
 * Copyright (C) 2015 c0ff3m4kr. <l34k@bk.ru>                                 *
 *----------------------------------------------------------------------------*/
#ifndef IPC_UNIX_OS_IPC_H_
#define IPC_UNIX_OS_IPC_H_
#include <string.h>
#include <stdio.h>
#include "global.h"
#include "ipcbase.h"

#ifndef IPC_IDENTIFIER_LENGTH
#error IPC_IDENTIFIER_LENGTH not defined!
#endif

namespace IPC {

class Stream: public IPC::StreamBase {
private:
    FILE *stream_;
public:
    Stream():
    	stream_(NULL)
    {}
    virtual ~Stream(){}
    explicit Stream(const Stream &other);
    const Stream &operator=(const Stream &other);
    ErrorCode write(const char *data, size_t bytes);
    ErrorCode read(char *buffer, size_t to_read);
#ifndef IPC_CLIENT_ONLY
    ErrorCode create();
#endif
#ifndef IPC_SERVER_ONLY
    ErrorCode open(const char *identifier);
#endif
    void close();
};
}  // namespace IPC

#endif  // IPC_UNIX_OS_IPC_H_
