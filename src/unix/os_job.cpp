#include "unix/os_job.h"
#include <errno.h>
#include <stdlib.h>
#include <sys/resource.h>
// fork & execv
#include <unistd.h>
namespace sandbox {

ErrorCode Job::setup() {
    return kSuccess;
}

ErrorCode Job::teardown() {
    return kSuccess;
}

ErrorCode Job::execute(const char *path, char * const argv[]) {
    struct rlimit limits;
    pid_t child_pid;
    // Fork
    child_pid = fork();
    if (child_pid == 0) {
        // We're the child
        if (user_time_limit_ != 0) {
            // Set CPU time limit (softlimit: round down, hardlimit:  rounded up to seconds)
            limits.rlim_cur = user_time_limit_ / 10;
            limits.rlim_max = (user_time_limit_ + 5)/ 10;
            setrlimit(RLIMIT_CPU, &limits);
        }
        if (memory_limit_ != 0) {
            limits.rlim_cur = memory_limit_;
            limits.rlim_max = memory_limit_;
            setrlimit(RLIMIT_AS, &limits);
        }
        // Exec
        execv(path, argv);
        // Should never get here
        abort();
    } else if (child_pid == -1) {
        // Failed
        return kOSError;
    } else {
        // We're the parent
        return kSuccess;
    }
}

}  // namespace sandbox
