/*----------------------------------------------------------------------------*
 * Copyright (C) 2015 c0ff3m4kr. <l34k@bk.ru>                                 *
 *----------------------------------------------------------------------------*/

#include "win/os_ipc.h"

namespace IPC {

Stream::Stream():
    named_pipe_(NULL)
{}

Stream::~Stream() {
    close();
}

ErrorCode IPC::Stream::write(const char *data, size_t bytes) {
    DWORD written;
    // OVERLAPPED overlapped = {0};
    if (FALSE == WriteFile(named_pipe_, data, bytes, &written, NULL)) {
        win_error_ = ::GetLastError();
        return kOSError;
    }
    if (written < bytes) {
        return kErrDataLeft;
    }
    return kSuccess;
}

ErrorCode IPC::Stream::read(char *buffer, size_t to_read) {
    DWORD read;
    if (FALSE == ReadFile(named_pipe_, buffer, to_read, &read, NULL)) {
        win_error_ = ::GetLastError();
        return kOSError;
    }
    if (read < to_read) {
        return kErrDataLeft;
    }
    return kSuccess;
}

#ifndef IPC_CLIENT_ONLY
ErrorCode Stream::create() {
    generateUniqueId_();
    FullName full_name;
    SECURITY_ATTRIBUTES sec_attrs = {0, NULL, false};
    get_pipe_name_(full_name, identifier_);
    // TODO(c0ff3m4kr): Use FILE_FLAG_OVERLAPPED
    named_pipe_ = ::CreateNamedPipe(full_name,
        PIPE_ACCESS_DUPLEX | FILE_FLAG_FIRST_PIPE_INSTANCE | WRITE_DAC,
        PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_NOWAIT,
        2, IPC_MAX_CONTENT_LENGTH, IPC_MAX_CONTENT_LENGTH, 0, &sec_attrs);
    if (named_pipe_ == INVALID_HANDLE_VALUE) {
        return kOSError;
    }
    return kSuccess;
}
#endif  // !IPC_SERVER_ONLY

#ifndef IPC_SERVER_ONLY
ErrorCode Stream::open(const char *identifier) {
    if(identifier == NULL) {
        return kErrInvalidArgument;
    }
    FullName full_name;
    SECURITY_ATTRIBUTES sec_attrs = {0, NULL, false};
    get_pipe_name_(full_name, identifier);
    named_pipe_ = CreateFile(full_name, GENERIC_READ | GENERIC_WRITE,
                             0,  // Greedy! We won't share
                             &sec_attrs,
                             OPEN_EXISTING,
                             SECURITY_ANONYMOUS,
                             NULL);
    if (named_pipe_ == INVALID_HANDLE_VALUE) {
        win_error_ = ::GetLastError();
        return kOSError;
    }
    return kSuccess;
}
#endif  // !IPC_SERVER_ONLY

void Stream::close() {
    if (named_pipe_ != NULL) {
        ::CloseHandle(named_pipe_);
        named_pipe_ = NULL;
    }
}


}  // namespace IPC
