/*----------------------------------------------------------------------------*
 * Copyright (C) 2015 c0ff3m4kr. <l34k@bk.ru>                                 *
 *----------------------------------------------------------------------------*/
#ifndef IPC_WIN_OS_IPC_H_
#define IPC_WIN_OS_IPC_H_
#include <string.h>
#include <windows.h>
#include "global.h"
#include "ipcbase.h"

#ifndef IPC_IDENTIFIER_LENGTH
#error IPC_IDENTIFIER_LENGTH not defined!
#endif
#if IPC_IDENTIFIER_LENGTH > 246
#error IPC Identifier to long. Can't create named pipes with this length.
#endif

namespace IPC {

static const char kIpcPipePrefix[] = "\\\\.\\pipe\\";

class Stream: public IPC::StreamBase {
private:
    typedef char FullName[IPC_IDENTIFIER_LENGTH + sizeof(kIpcPipePrefix) + 1];
    HANDLE named_pipe_;

    inline void get_pipe_name_(FullName name, const char *identifier) {
        strncpy_s(name, sizeof(FullName), kIpcPipePrefix, sizeof(kIpcPipePrefix));
        strcat_s(name, sizeof(FullName), identifier);
    }
    DWORD win_error_;


public:
    Stream();
    virtual ~Stream();
    ErrorCode write(const char *data, size_t bytes);
    ErrorCode read(char *buffer, size_t to_read);
#ifndef IPC_CLIENT_ONLY
    ErrorCode create(const char *identifier);
#endif
#ifndef IPC_SERVER_ONLY
    ErrorCode open(const char *identifier);
#endif
    void close();
};
}  // namespace IPC

#endif  // IPC_WIN_OS_IPC_H_
