/*----------------------------------------------------------------------------*
 * Copyright (C) 2015 c0ff3m4kr. <l34k@bk.ru>                                 *
 *----------------------------------------------------------------------------*/
#ifndef SANDBOX_WINDOWS_SANDBOX_H_
#define SANDBOX_WINDOWS_SANDBOX_H_

#include <Windows.h>
#include "global.h"
#include "jobbase.h"

#define MAX_PROCESSES 10

namespace sandbox {

class Job: public JobBase {

private:
    typedef JOBOBJECT_EXTENDED_LIMIT_INFORMATION LimitStruct;

    // Second, set some UI restrictions
    JOBOBJECT_BASIC_UI_RESTRICTIONS jo_uir_;
    JOBOBJECT_SECURITY_LIMIT_INFORMATION jo_secl_;
    LimitStruct jo_limit_;
    HANDLE job_object_;
    HDESK desktop_;
    DWORD win_error_;

protected:
    ErrorCode setup();
    ErrorCode teardown();
    ErrorCode addProcess(void* pProcess);

public:
    Job();
    inline void setActiveProcessLimit(unsigned int limit) {
        if (limit == 0) {
            removeActiveProcessLimit();
        }else{
            jo_limit_.BasicLimitInformation.ActiveProcessLimit = limit;
            jo_limit_.BasicLimitInformation.LimitFlags |= JOB_OBJECT_LIMIT_ACTIVE_PROCESS;
        }
    }

    inline void removeActiveProcessLimit() {
        jo_limit_.BasicLimitInformation.LimitFlags &= ~JOB_OBJECT_LIMIT_ACTIVE_PROCESS;
    }

    inline unsigned int activeProcessLimit() const{
        if(jo_limit_.BasicLimitInformation.LimitFlags & JOB_OBJECT_LIMIT_ACTIVE_PROCESS){
            return jo_limit_.BasicLimitInformation.ActiveProcessLimit;
        } else {
            return 0;
        }
    }

    inline void setUserTimeLimit(unsigned int limit_100ms) {
        if (limit_100ms == 0) {
            removeUserTimeLimit();
        } else {
            ULONGLONG limit;
            limit = limit_100ms * 100;
            if ((unsigned) jo_limit_.BasicLimitInformation.PerProcessUserTimeLimit.QuadPart != limit) {
                jo_limit_.BasicLimitInformation.PerProcessUserTimeLimit.QuadPart = limit;
            }
            jo_limit_.BasicLimitInformation.LimitFlags |= JOB_OBJECT_LIMIT_PROCESS_TIME;
        }
    }

    inline void removeUserTimeLimit() {
        jo_limit_.BasicLimitInformation.LimitFlags &= ~JOB_OBJECT_LIMIT_PROCESS_TIME;
    }

    inline unsigned int userTimeLimit() const {
        unsigned int result;
        if (jo_limit_.BasicLimitInformation.LimitFlags & JOB_OBJECT_LIMIT_PROCESS_TIME) {
            result = (unsigned int) jo_limit_.BasicLimitInformation.PerProcessUserTimeLimit.QuadPart / 100;
        } else {
            result = 0;
        }
        return result;
    }

    inline size_t memoryLimit() const {
        if (jo_limit_.BasicLimitInformation.LimitFlags & JOB_OBJECT_LIMIT_PROCESS_MEMORY) {
            return jo_limit_.ProcessMemoryLimit;
        } else {
            return 0;
        }
    }

    inline void setMemoryLimit(size_t limit) {
        if (jo_limit_.ProcessMemoryLimit != limit) {
            jo_limit_.ProcessMemoryLimit = limit;
        }
        jo_limit_.BasicLimitInformation.LimitFlags |= JOB_OBJECT_LIMIT_PROCESS_MEMORY;
    }

    ErrorCode execute(const char * path, char * const argv[]);
};
}

#endif
