/*----------------------------------------------------------------------------*
 * Copyright (C) 2015 c0ff3m4kr. <l34k@bk.ru>                                 *
 *----------------------------------------------------------------------------*/

#include "win/os_job.h"

namespace sandbox {

ErrorCode Job::setup() {
    // desktop_name = L"GenerateUniqueNameHere_321321";

    ErrorCode result;
    DWORD desktop_flags = DESKTOP_CREATEWINDOW;  // Has to be enabled (see doc)
    desktop_flags |= DELETE;

    // desktop_ = CreateDesktop(desktop_name, NULL, NULL, 0, desktop_flags, NULL);
    if (desktop_ == NULL) {
        result = kOSError;
        win_error_ = ::GetLastError();
    } else {
        if (NULL == ((job_object_ = ::CreateJobObject(NULL, NULL)))
            || (FALSE == ::SetInformationJobObject(job_object_, ::JobObjectExtendedLimitInformation,
                                                   &jo_limit_, sizeof(jo_limit_)))
            || (FALSE == ::SetInformationJobObject(job_object_, ::JobObjectBasicUIRestrictions,
                                                   &jo_uir_,   sizeof(jo_uir_)))
            || (FALSE == ::SetInformationJobObject(job_object_, ::JobObjectSecurityLimitInformation, &jo_secl_,
                                                   sizeof(jo_secl_)))) {
            win_error_ = ::GetLastError();
            result = kOSError;
        } else {
            result = kSuccess;
        }
    }
    return result;
}

ErrorCode Job::teardown() {
    if (job_object_ == NULL) {
        return kSuccess;
    }

    // To compile an application that uses this function,
    // define _WIN32_WINNT as 0x0500 or later.
    // For more information, see Using the Windows Headers.
    if (0 == ::TerminateJobObject(job_object_, 0)) {
        // Termination failed
        win_error_ = GetLastError();
        return kOSError;
    }

    (void) CloseHandle(job_object_);
    job_object_ = NULL;
    return kSuccess;
}

ErrorCode Job::addProcess(void* pProcess) {
    HANDLE proc = (HANDLE) pProcess;
    if (job_object_ == NULL) {
        return kStateError;
    }
    if (0 != ::AssignProcessToJobObject(job_object_, proc)) {
        win_error_ = ::GetLastError();
        return kOSError;
    }
    return kSuccess;
}

Job::Job():
    job_object_(NULL),
    desktop_(NULL) {
    // Fill all limit/restriction structures with zeros
    ::memset(&jo_limit_, 0, sizeof(jo_limit_));
    ::memset(&jo_uir_,   0, sizeof(jo_uir_));
    ::memset(&jo_secl_,  0, sizeof(jo_secl_));

    // Prevents any process in the job from using a token that specifies
    // the local administrators group.
    jo_secl_.SecurityLimitFlags = JOB_OBJECT_SECURITY_NO_ADMIN;


    jo_uir_.UIRestrictionsClass  = JOB_OBJECT_UILIMIT_NONE;    // A fancy 0
    // The process can't logoff the system
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_EXITWINDOWS;
    // Prevents processes from reading the clipboard
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_READCLIPBOARD;
    // Prevents processes from erasing the clipboard
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_WRITECLIPBOARD;
    // Prevents processes from changing system parameters via the
    // SystemParametersInfo function
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_SYSTEMPARAMETERS;
    // Prevents processes from changing the display settings via the
    // ChangeDisplaySettings function
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_DISPLAYSETTINGS;
    // Gives the job its own global atom table and restricts processes in the
    // job to accessing only the job's table
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_GLOBALATOMS;
    // Prevents processes from creating/switching desktops via the
    // CreateDesktop/SwitchDesktop functions
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_DESKTOP;
    // The process can't access USER object (like other windows) in the system
    jo_uir_.UIRestrictionsClass |= JOB_OBJECT_UILIMIT_HANDLES;
}

ErrorCode Job::execute(const char *path, char * const argv[])
{
    CreateProcess();
}

}  // namespace sandbox
