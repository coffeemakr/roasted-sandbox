#ifndef GLOBAL_H_
#define GLOBAL_H_

#ifdef _MSC_VER
#include "win/stdint.h"
#else
#include <stdint.h>
#endif

#include <cstddef>

enum ErrorCode{
    kSuccess     = 0x00,
    kOSError     = 0x01,
    kStateError  = 0x02,
    kMemoryError = 0x03,
    kNoData      = 0x04,
    kInvalidMsg  = 0x05,
    kErrFileNotFound = 0x06,
    kErrPipeBusy     = 0x07,
	kErrDataLeft     = 0x08,
    kErrStringTooLong = 0x09,
    kErrInvalidArgument = 0x10,
    kErrorMask   = 0xFF
};

enum IPCSignals{
    kIpcSigDie       = 0x0100,
    kIpcSigMask      = 0xCF00
};

enum MessageCode {
    kRequestFlag     = 0x1000,
    kResponseFlag    = 0x2000,
    kMessageCodeMask = 0xFFFF
};

#endif  // GLOBAL_H_
