#ifndef SANDBOX_JOB_H_
#define SANDBOX_JOB_H_

#include "global.h"

namespace sandbox {
class JobBase
{
protected:
    virtual ErrorCode setup() = 0;
    virtual ErrorCode teardown() = 0;
    virtual ErrorCode addProcess(void* pProcess) = 0;

public:
    virtual ~JobBase();
    /**
     * @brief Set the maximum number of active processes.
     * @param[in] limit The limit to set.
     */
    virtual void setActiveProcessLimit(unsigned int limit) = 0;
    /**
     * @brief Removes the limit of miximum number of active processes.
     */
    virtual void removeActiveProcessLimit() = 0;
    /**
     * @brief Get maximum numbers of active processes.
     * @return The maximum number of active processes or 0 if no limit is set.
     */
    virtual unsigned int activeProcessLimit() const = 0;
    /**
     * @brief setUserTimeLimit
     * @param[in] limit_100ms Limit process user time in 100ms.
     */
    virtual void setUserTimeLimit(unsigned int limit_100ms) = 0;
    /**
     * @brief removeUserTimeLimit
     */
    virtual void removeUserTimeLimit() = 0;
    /**
     * @brief userTimeLimit
     * @return The currently set user time limit or 0 if no limit is set.
     */
    virtual unsigned int userTimeLimit() const = 0;
    /**
     * @brief memoryLimit
     * @return The maximum memory or 0 if no limit is set.
     */
    virtual size_t memoryLimit() const = 0;
    /**
     * @brief setMemoryLimit
     * @param[in] limit Memory limit.
     */
    virtual void setMemoryLimit(size_t limit) = 0;
    /**
     * @brief execute
     * @param[in] path Path to the executable
     * @param[in] argv List of arguments teminated with a NULL.
     * @return Error code. On success kSuccess is returned.
     */
    virtual ErrorCode execute(const char * path, char * const argv[]) = 0;
};
}  // namespace sandbox
#endif  // SANDBOX_JOB_H_
