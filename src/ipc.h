#ifndef IPC_C_
#define IPC_C_
#include <stdlib.h>
#include "global.h"
#include "os_ipc.h"

namespace IPC {

#define IPC_MAKE_VERSION(a,b,c,d) (((a & 0xFF) << (3*8)) + ((b & 0xFF) << (2*8)) + ((c & 0xFF) << (1*8)) + (a & 0xFF))

static const uint32_t kMessageVersion = IPC_MAKE_VERSION(1,0,0,0);

struct MessageHead
{
    uint32_t version;
    uint32_t code;
    uint32_t content_size;
};

class Communication {

private:
    IPC::Stream stream;
    char * buffer_;
    size_t buffer_size_;
    MessageHead received_header_;

protected:
    inline ErrorCode allocBuffer(size_t size) {
        if (size > IPC_MAX_CONTENT_LENGTH) {
            return kErrInvalidArgument;
        }
        if (buffer_size_ < size) {
            freeBuffer();
            buffer_ = static_cast<char *>(calloc(1, size + 1));
            if (buffer_ == NULL) {
                return kMemoryError;
            } else {
                buffer_size_ = size;
            }
        }
        return kSuccess;
    }

    inline void freeBuffer(){
        if (buffer_ != NULL) {
            free(buffer_);
            buffer_ = NULL;
            buffer_size_ = 0;
        }
    }


public:
    Communication():
        stream(),
        buffer_(NULL),
        buffer_size_(0),
        received_header_()
    {
        received_header_.version = 0;
        received_header_.code = 0;
        received_header_.content_size = 0;
    }

    explicit inline Communication(const Communication &other):
        stream(),
        buffer_(NULL),
        buffer_size_(0),
        received_header_()
    {
        stream = Stream(other.stream);
        ::memcpy(&received_header_, &other.received_header_, sizeof(received_header_));
        if (received_header_.content_size > 0) {
            if (other.buffer_size_ > 0 && other.buffer_ != NULL) {
                if(allocBuffer(other.buffer_size_) != kSuccess) {
                    // TODO: Errorhandling
                }else{
                    ::memcpy(buffer_, other.buffer_, other.buffer_size_);
                }
            }
        }
    }

    inline const Communication &operator=(const Communication& other) {
        Communication tmp(other);
        buffer_size_ = tmp.buffer_size_;
        buffer_ = tmp.buffer_;
        received_header_ = tmp.received_header_;
        stream = tmp.stream;
        return *this;
    }
    virtual ~Communication();

    #ifndef IPC_SERVER_ONLY
    inline ErrorCode open(const char *identifier) {
       return stream.open(identifier);
    }
    #endif  // IPC_SERVER_ONLY

    #ifndef IPC_CLIENT_ONLY
    inline const char *clientIdentifier() {
        return stream.clientIdentifier();
    }
    inline ErrorCode create() {
       return stream.create();
    }
    #endif  // IPC_CLIENT_ONLY

    inline ErrorCode send(uint32_t sig) {
        MessageHead head = {kMessageVersion, kRequestFlag | sig, 0};
        return stream.write(reinterpret_cast<char*>(&head), sizeof(MessageHead));
    }

    inline ErrorCode send(uint32_t sig, const char *content, uint32_t content_size) {
        // TODO: Handle IPC_MAX_CONTENT_LENGTH
        MessageHead head = {kMessageVersion, kRequestFlag | sig, content_size};
        ErrorCode err = stream.write(reinterpret_cast<char*>(&head), sizeof(MessageHead));
        if (err == kSuccess && content_size > 0) {
            err = stream.write(content, content_size);
        }
        return err;
    }

    inline ErrorCode receive() {
        MessageHead head;
        // TODO: Handle IPC_MAX_CONTENT_LENGTH
        ErrorCode err = stream.read(reinterpret_cast<char*>(&head), sizeof(MessageHead));
        if (err == kSuccess && head.version != kMessageVersion) {
            err = kInvalidMsg;
        }
        if (err == kSuccess && head.content_size > 0) {
            err = allocBuffer(head.content_size);
            if (err == kSuccess) {
                err = stream.read(buffer_, head.content_size);
            }
        }
        if (err == kSuccess) {
            // Copy
            received_header_ = head;
        }
        return err;
    }
};
}
#endif  // IPC_C_
