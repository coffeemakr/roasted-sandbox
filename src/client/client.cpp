#include "client.h"

#ifdef SANDBOX_CLIENT_NO_MAIN
#warning There should probably never be a client without a predefined main!
#else
int main(int argc, const char* argv[]) {
    return sandbox::Client::main(argc, argv);
}
#endif

namespace sandbox {

    Client::Client():
        com()
    {}

    int Client::main(int argc, const char *argv[]) {
        Client client;
        if(argc != 1 || argv[1] == NULL) {
            // TODO(c0ff3m4kr): Error handling
            return 1;
        }
        if(kSuccess != client.ipcOpen(argv[0])) {
            // TODO(c0ff3m4kr): Error handling
            return 1;
        }
        return 0;
    }

}  // namespace sandbox
