#ifndef SANDBOX_CLIENT_H_
#define SANDBOX_CLIENT_H_

#include "global.h"
#include "ipc.h"

#ifdef IPC_SERVER_ONLY
#error client.h included for server only!
#endif
#ifndef IPC_CLIENT_ONLY
#error client.h included for client only!
#endif

int main(int argc, const char* argv[]);

namespace sandbox {
class Client {
private:
	IPC::Communication com;
public:
    Client();
    static int main(int argc, const char *argv[]);
    inline ErrorCode ipcOpen(const char* identifier) {
        return com.open(identifier);
    }
};
}  // namespace sandbox

#endif  // SANDBOX_CLIENT_H_
