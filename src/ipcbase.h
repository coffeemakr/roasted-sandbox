#ifndef IPC_IPCBASE_H_
#define IPC_IPCBASE_H_
#include <cstdio>
#include "global.h"
#define  IPC_IDENTIFIER_LENGTH 32
#define  IPC_MAX_CONTENT_LENGTH 2048

namespace IPC {
/**
    * @brief IPC communication stream
    * @details Base class for OS dependend communication stream implementation.
    */
class StreamBase {
protected:
    char identifier_[IPC_IDENTIFIER_LENGTH];
    static unsigned long int unique_id_;
    void generateUniqueId_() {
        ++unique_id_;
        std::snprintf(identifier_, IPC_IDENTIFIER_LENGTH, "%lx", unique_id_);
    }
public:
    virtual ~StreamBase();
    /**
        * @brief Write data to the stream.
        * @param data The data to write
        * @param bytes Size of the data in bytes.
        * @return Error code
        */
    virtual ErrorCode write(const char *data, size_t bytes) = 0;
    /**
        * @brief Read data from the stream.
        * @param buffer The data to write
        * @param buffer_size Size of the data in bytes.
        * @return Error code
        */
    virtual ErrorCode read(char *buffer, size_t buffer_size) = 0;
#ifndef IPC_SERVER_ONLY
	virtual ErrorCode open(const char *identifier) = 0;
#endif
#ifndef IPC_CLIENT_ONLY
	virtual ErrorCode create() = 0;
    inline const char *clientIdentifier() {
        return identifier_;
    }
#endif
    virtual void close() = 0;
};
}  // namespace IPC
#endif  // IPC_IPCBASE_H_
