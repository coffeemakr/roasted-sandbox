#ifndef BROKER_H
#define BROKER_H
#include <new>          // std::nothrow
#include <list>
#include "global.h"
#include "job.h"
#include "ipc.h"

#ifdef IPC_CLIENT_ONLY
#error broker included for client only!
#endif
#ifndef IPC_SERVER_ONLY
#error broker included not for sever only!
#endif


namespace sandbox {
class Broker
{
public:
    class ProcessInfo {
    public:
        IPC::Communication com;
        ProcessInfo():
            com()
        {}
    };
    typedef std::list<ProcessInfo*>::iterator ProcessIdentifier;

private:
    sandbox::Job job_;
    std::list<ProcessInfo*> processes_;

public:
    Broker():
        job_(),
        processes_()
        {}
    ~Broker()
        {}
    /**
     * @brief Runs an application in the associated sandbox.
     *
     * @param [in]path Path to the executable
     * @param [in]prog Name of the application (Passed as argument 0.)
     * @param [out]proc_ident Identifier to the created process.
     * @return Error code.
     */
    ErrorCode run(const char* path, const char *prog, ProcessIdentifier* proc_ident){
        ErrorCode e = kSuccess;
        ProcessInfo *proc;
        const char * argv[3] = {prog, NULL, NULL};
        if (prog == NULL) {
            return kErrInvalidArgument;
        }
        proc = new (std::nothrow) ProcessInfo();
        if (proc == NULL) {
            return kMemoryError;
        }
        // Create communication port
        if (kSuccess == (e = proc->com.create())) {
            // Pass communication identifier as the argument
            if (NULL == (argv[1] = proc->com.clientIdentifier())) {
                e = kStateError;
            }
        }
        try {
            processes_.push_back(proc);
            *proc_ident = --processes_.end();
        } catch(std::bad_alloc) {
            e = kMemoryError;
        }
        if (e == kSuccess) {
            e = job_.execute(path, const_cast<char **>(argv));
            if (e != kSuccess) {
                *proc_ident = processes_.end();
                delete processes_.back();
                processes_.pop_back();
            }
        }
        return e;
    }
};
}  // namespace sandbox
#endif // BROKER_H
