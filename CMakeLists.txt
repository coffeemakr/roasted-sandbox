project(sandbox)
cmake_minimum_required(VERSION 2.8)
# scan-build --use-cc=/usr/bin/clang -enable-checker alpha -enable-checker security -enable-checker llvm

option(WERROR   "Quit compilation if the compiler finds a warning" ON)
option(OPTIMIZE "Optimize the compilation. This will be always on when building the release." OFF)

# CLANG:
#-Wextra-semi
#-Wnon-virtual-dtor
## -Wpadded
#-Wdocumentation

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    set(COMPILER_CXX_WALL "/W4")
    set(COMPILER_C_WALL "/W4")
    set(COMPILER_WERROR "/WX")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    # Gnu / Clang
    set(COMMON_FLAGS "-Wall -Wextra -Weffc++ -Wabi -Wpedantic -Wparentheses -Wsequence-point -Wuninitialized")
    set(C_FLAGS "${COMMON_FLAGS} -Wc++-compat")
    set(CXX_FLAGS "${COMMON_FLAGS} -Wold-style-cast -Woverloaded-virtual -Wnon-virtual-dtor -Wctor-dtor-privacy -Wc++11-compat" )
    set(COMPILER_WERROR "-Werror")
    set(COMPILER_OPTIMIZE "-O3")
    if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        # Gnu specific
        set(C_FLAGS   "-ansi ${C_FLAGS}")
        set(CXX_FLAGS "-ansi ${CXX_FLAGS}")
    else()
        # Clang specific
        set(C_FLAGS   "-Weverything -Wno-padded --analyze ${C_FLAGS}")
        set(CXX_FLAGS "-Weverything -Wno-padded --analyze ${CXX_FLAGS}")
    endif()
endif()

if(WERROR)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMPILER_WERROR}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMPILER_WERROR}")
endif(WERROR)

set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${COMPILER_OPTIMIZE}")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${COMPILER_OPTIMIZE}")
if(OPTIMIZE)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMPILER_OPTIMIZE}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMPILER_OPTIMIZE}")
endif(OPTIMIZE)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS}")

if( WIN32 )
set(sandbox_OS_SUBDIR win)
else ( WIN32)
set(sandbox_OS_SUBDIR unix)
endif( WIN32 )

include_directories(src/)
include_directories(src/${sandbox_OS_SUBDIR})
aux_source_directory(src/ SOURCES)
aux_source_directory("src/${sandbox_OS_SUBDIR}" SOURCES)
aux_source_directory(src/broker sandbox_broker_SOURCES)
aux_source_directory(src/client sandbox_client_SOURCES)

#add_definitions(-DIPC_CLIENT_ONLY)$
add_library(sandbox_client ${SOURCES} ${sandbox_client_SOURCES})
SET_TARGET_PROPERTIES(sandbox_client PROPERTIES COMPILE_FLAGS -DIPC_CLIENT_ONLY)
#remove_definitions(-DIPC_CLIENT_ONLY)
#add_definitions(-DIPC_SERVER_ONLY)
add_library(sandbox_broker ${SOURCES} ${sandbox_broker_SOURCES})
SET_TARGET_PROPERTIES(sandbox_broker PROPERTIES COMPILE_FLAGS -std=c++11)
SET_TARGET_PROPERTIES(sandbox_broker PROPERTIES COMPILE_FLAGS -DIPC_SERVER_ONLY)
#remove_definitions(-DIPC_SERVER_ONLY)

