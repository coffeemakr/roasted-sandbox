# Roasted Sandbox

## Sandboxing
The sandboxing implements a way spawn new processes in a restricted environment.

### Windows
On Windows the security of the sandbox depends on the following components provided by the operating system:

  - JobObject
  - Tokens


### Unix

Some ideas on restricting the processes on Unix:   
- https://github.com/pshved/timeout
- [Limiting time and memory consumption of a program in Linux][time_mem_limit_unix]
- ulimit - get and set user limits (deprecated)
- setrlimit
- sysconf

System specific restrictions:
  - [ ] apparmor rules
  - [ ] SELinux policy: 

## IPC

### Windows
On Windows a named pipe is used for the communication between the processes.

### Unix
On Unix the command socketpair is used to generate two sockets.

## Further readings
* [Job Kernel Objects (Windows)][msj_jobkernelobj]: Pretty good article about restricting a process using JobObjects.
* [Named Pipe attacks (Windows)][windows_namedpipe_attack]
* [Named Pipe attacks (Unix)][unix_namedpipe_attack]

[msj_jobkernelobj]: http://www.microsoft.com/msj/0399/jobkernelobj/jobkernelobj.aspx "Make Your Windows 2000 Processes Play Nice Together With Job Kernel Objects"
[unix_namedpipe_attack]: http://insecure.org/sploits/named.pipes.attack.html "Named Pipe Attack"
[windows_namedpipe_attack]: http://www.blakewatts.com/namedpipepaper.html
[time_mem_limit_unix]: http://coldattic.info/shvedsky/pro/blogs/a-foo-walks-into-a-bar/posts/40 "Time and memory consumption of a program in Linux"
